#include <gtest/gtest.h>
#include <file_png.h>
#include <pixel_formats.hpp>

#include <cmath>
#include <filesystem>
#include <limits>
#include <memory>
#include <stdexcept>
#include <tuple>

#ifndef M_PI_DBL
#    define M_PI_DBL 3.14159265358979323846264338327950288
#endif

namespace png_io {
using namespace voxels;

class PngFullTest : public ::testing::Test {
protected:
    PngFullTest() {}
};

// Test file_png IO for commonly used formats
TEST_F(PngFullTest, RoundTripPngImageGreyAlpha8bit) {
    const std::string file_name("test_write_png_image_grey_alpha8.png");
    constexpr unsigned w = 256;
    constexpr unsigned h = 256;
    constexpr unsigned bit_depth = 8;
    constexpr unsigned stride = 2;

    std::vector<unsigned char> image;
    image.resize((w * stride) * h);

    for (int y = 0; y < h; y++) {
        for (int x = 0; x < (w * stride); x += stride) {
            size_t base_idx = (y * (static_cast<size_t>(w) * stride)) + x;
            PNG_IO_GREY_ALPHA8& pixel = (PNG_IO_GREY_ALPHA8&)image.data()[base_idx];

            pixel.g = ((x / stride % 32) * 4) + ((y % 32) * 4);
            pixel.a = (x / stride + y) / 2;
        }
    }

    const auto error = lodepng_encode_file(file_name.c_str(), image.data(), w, h, LCT_GREY_ALPHA, bit_depth);
    if (error > 0) {
        std::cout << "Error:  " << lodepng_error_text(error) << std::endl;
    }

    EXPECT_EQ(error, 0);

    unsigned char* out = nullptr;
    unsigned w_out;
    unsigned h_out;

    const auto read_error = lodepng_decode_file(&out, &w_out, &h_out, file_name.c_str(), LCT_GREY_ALPHA, bit_depth);
    if (read_error > 0) {
        std::cout << "Read Error:  " << lodepng_error_text(read_error) << std::endl;
    }

    size_t mismatch_count(0);
    for (size_t i = 0; i < (static_cast<size_t>(w_out) * stride) * h_out; i++) {
        if (image.data()[i] != out[i]) {
            mismatch_count++;
        }
    }
    EXPECT_EQ(mismatch_count, 0);
}

TEST_F(PngFullTest, RoundTripPngImageGrey8bit) {
    const std::string file_name("test_write_png_image_grey8.png");

    constexpr unsigned w = 256;
    constexpr unsigned h = 256;
    constexpr unsigned bit_depth = 8;
    constexpr unsigned stride = 1;

    std::vector<unsigned char> image;
    image.resize((w * stride) * h);

    for (int y = 0; y < h; y++) {
        for (int x = 0; x < (w * stride); x += stride) {
            size_t base_idx = (y * (static_cast<size_t>(w) * stride)) + x;
            PNG_IO_GREY8& pixel = (PNG_IO_GREY8&)image.data()[base_idx];

            pixel.g = ((x % 32) * 4) + ((y % 32) * 4);
        }
    }

    const auto error = lodepng_encode_file(file_name.c_str(), image.data(), w, h, LCT_GREY, bit_depth);
    if (error > 0) {
        std::cout << "Error:  " << lodepng_error_text(error) << std::endl;
    }

    EXPECT_EQ(error, 0);

    unsigned char* out = nullptr;
    unsigned w_out;
    unsigned h_out;

    const auto read_error = lodepng_decode_file(&out, &w_out, &h_out, file_name.c_str(), LCT_GREY, bit_depth);
    if (read_error > 0) {
        std::cout << "Read Error:  " << lodepng_error_text(read_error) << std::endl;
    }

    size_t mismatch_count(0);
    for (size_t i = 0; i < (static_cast<size_t>(w_out) * stride) * h_out; i++) {
        if (image.data()[i] != out[i]) {
            mismatch_count++;
        }
    }
    EXPECT_EQ(mismatch_count, 0);
}

TEST_F(PngFullTest, RoundTripPngImageRGB) {
    const std::string file_name("test_write_png_image_rgb.png");
    constexpr unsigned w = 256;
    constexpr unsigned h = 256;
    constexpr unsigned bit_depth = 8;
    constexpr unsigned stride = 3;

    std::vector<unsigned char> image;
    image.resize((w * stride) * h);

    for (int y = 0; y < h; y++) {
        for (int x = 0; x < (w * stride); x += stride) {
            const size_t base_idx = (y * ((static_cast<size_t>(w) * stride))) + x;
            PNG_IO_RGB& pixel = (PNG_IO_RGB&)image.data()[base_idx];

            pixel.r = x / stride % 32 * 8;
            pixel.g = y % 32 * 8;
            pixel.b = 255 - ((pixel.r + pixel.g) / 2);
        }
    }

    const auto error = lodepng_encode_file(file_name.c_str(), image.data(), w, h, LCT_RGB, bit_depth);
    if (error > 0) {
        std::cout << "Error:  " << lodepng_error_text(error) << std::endl;
    }

    EXPECT_EQ(error, 0);

    unsigned char* out = nullptr;
    unsigned w_out;
    unsigned h_out;

    const auto read_error = lodepng_decode_file(&out, &w_out, &h_out, file_name.c_str(), LCT_RGB, bit_depth);
    if (read_error > 0) {
        std::cout << "Read Error:  " << lodepng_error_text(read_error) << std::endl;
    }

    size_t mismatch_count(0);
    for (size_t i = 0; i < (static_cast<size_t>(w_out) * stride) * h_out; i++) {
        if (image.data()[i] != out[i]) {
            mismatch_count++;
        }
    }
    EXPECT_EQ(mismatch_count, 0);
}

TEST_F(PngFullTest, RoundTripPngImageRGBA) {
    const std::string file_name("test_write_png_image_rgba.png");
    constexpr unsigned w = 256;
    constexpr unsigned h = 256;
    constexpr unsigned bit_depth = 8;
    constexpr unsigned stride = 4;

    std::vector<unsigned char> image;
    image.resize((w * stride) * h);

    for (size_t y = 0; y < h; y++) {
        for (size_t x = 0; x < (static_cast<size_t>(w) * stride); x += stride) {
            size_t base_idx = (y * (static_cast<size_t>(w) * stride)) + x;

            PNG_IO_RGBA& pixel = (PNG_IO_RGBA&)image.data()[base_idx];

            pixel.r = (x / stride % 32) * 8;
            pixel.g = (y % 32) * 8;
            pixel.b = 255 - ((pixel.r + pixel.g) / 2);
            pixel.a = static_cast<unsigned char>((x / stride + y) / 2);
        }
    }

    const auto write_error = lodepng_encode_file(file_name.c_str(), image.data(), w, h, LCT_RGBA, bit_depth);
    if (write_error > 0) {
        std::cout << "Write Error:  " << lodepng_error_text(write_error) << std::endl;
    }

    EXPECT_EQ(write_error, 0);

    unsigned char* out = nullptr;
    unsigned w_out;
    unsigned h_out;

    const auto read_error = lodepng_decode_file(&out, &w_out, &h_out, file_name.c_str(), LCT_RGBA, bit_depth);
    if (read_error > 0) {
        std::cout << "Read Error:  " << lodepng_error_text(read_error) << std::endl;
    }

    size_t mismatch_count(0);
    for (size_t i = 0; i < (static_cast<size_t>(w_out) * stride) * h_out; i++) {
        if (image.data()[i] != out[i]) {
            mismatch_count++;
        }
    }
    EXPECT_EQ(mismatch_count, 0);
}

}  // namespace png_io
