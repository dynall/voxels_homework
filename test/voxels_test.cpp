#include <gtest/gtest.h>

#include <filesystem>  // A C++17 capable compiler is assumed here.
#include <optional>
#include <string>
#include <tuple>

#include <voxels.hpp>

namespace voxels {

class VoxelDatabaseTest : public ::testing::Test {
protected:
    VoxelDatabaseTest() :
            voxel_db_(IVoxelDatabase::create()){

        voxel_db_->initalize_database({ 256, 256, 256 }, { -1.f, -1.f, -1.f }, { 2.f, 2.f, 2.f });
    }

    std::unique_ptr<IVoxelDatabase> voxel_db_;
};


TEST_F(VoxelDatabaseTest, CreateDatabase) {

    Size3d32 size(voxel_db_->get_size());
    Point3d32 min_corner(voxel_db_->get_min_corner());
    Dimensions3d32 dim(voxel_db_->get_dimensions());

    Dimensions3d32 voxel_dim(voxel_db_->get_voxel_dimensions());


    EXPECT_EQ(size.x, 256);
    EXPECT_EQ(size.y, 256);
    EXPECT_EQ(size.z, 256);

    EXPECT_EQ(min_corner.x, -1);
    EXPECT_EQ(min_corner.y, -1);
    EXPECT_EQ(min_corner.z, -1);

    EXPECT_EQ(dim.x, 2.f);
    EXPECT_EQ(dim.y, 2.f);
    EXPECT_EQ(dim.z, 2.f);

    EXPECT_EQ(voxel_dim.x, 2.f / 256.f);
    EXPECT_EQ(voxel_dim.y, 2.f / 256.f);
    EXPECT_EQ(voxel_dim.z, 2.f / 256.f);
}


}  // namespace voxels
