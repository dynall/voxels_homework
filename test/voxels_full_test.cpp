#include "voxels.hpp"
#include "file_png.h"
#include "pixel_formats.hpp"

#include <gtest/gtest.h>

#include <cmath>
#include <filesystem>  // A C++17 capable compiler is assumed here.
#include <limits>
#include <optional>
#include <string>
#include <string_view>
#include <tuple>

namespace voxels {

class VoxelDatabaseFullTest : public ::testing::Test {
protected:
    VoxelDatabaseFullTest() : voxel_db_(IVoxelDatabase::create()) {}

    float clamp(float f, float fmin, float fmax) {
        return std::max(std::min(f, fmax), fmin);
    }

    void WriteToImageRGB(const std::string& file_name, float z_layer) {
        const Point3d32 db_corner = voxel_db_->get_min_corner();
        const Dimensions3d32 voxel_size = voxel_db_->get_voxel_dimensions();
        const Dimensions3d32 db_size = voxel_db_->get_dimensions();
        const unsigned w = voxel_db_->get_size().x;
        const unsigned h = voxel_db_->get_size().y;
        const unsigned bit_depth = 8;
        const unsigned stride = 3;

        std::vector<unsigned char> image;
        image.resize((w * stride) * h);

        float max_sdf = 2.f;

        for (size_t y = 0; y < h; y++) {
            for (size_t x = 0; x < (static_cast<size_t>(w) * stride); x += stride) {
                size_t base_idx = (y * (static_cast<size_t>(w) * stride)) + x;

                const Point3d32 position(db_corner.x + (voxel_size.x * x / stride), db_corner.y + (voxel_size.y * y),
                                         z_layer);
                float sdf = voxel_db_->sdf_at_point(position);

                PNG_IO_RGB& pixel = (PNG_IO_RGB&)image.data()[base_idx];

                pixel.r = (sdf > 0.f) ? 255 - static_cast<unsigned char>(clamp(fabs(sdf) / max_sdf, 0.f, 1.f) * 255.f)
                                      : (fabs(sdf) <= (voxel_size.x * 0.6))
                                                ? 255 - static_cast<unsigned char>(
                                                                clamp(fabs(sdf) / (voxel_size.x), 0.f, 1.f) * 255.f)
                                                : 0;
                pixel.g = 0;
                pixel.b = (sdf < 0.f) ? 255 - static_cast<unsigned char>(clamp(fabs(sdf) / max_sdf, 0.f, 1.f) * 255.f)
                                      : (fabs(sdf) <= (voxel_size.x * 0.6f))
                                                ? 255 - static_cast<unsigned char>(
                                                                clamp(fabs(sdf) / (voxel_size.x), 0.f, 1.f) * 255.f)
                                                : 0;
            }
        }

        const auto error = lodepng_encode_file(file_name.c_str(), image.data(), w, h, LCT_RGB, bit_depth);
    }

    std::unique_ptr<IVoxelDatabase> voxel_db_;
};

TEST_F(VoxelDatabaseFullTest, VoxelizeSphere) {
    voxel_db_->initalize_database({256, 256, 256}, {-2.f, -2.f, -2.f}, {4.f, 4.f, 4.f});
    // half the distance to the corner of a square voxel

    bool results = voxel_db_->voxelize_sphere(VoxelOperation::kOverwrite, {0.0f, 0.0f, 0.0f}, 1.f);

    if (results) {
        EXPECT_LT(voxel_db_->sdf_at_point({-1.f, 0.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({1.f, 0.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, -1.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, 1.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, 0.f, -1.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, 0.f, 1.0f}), voxel_db_->get_tolerance());

        WriteToImageRGB("voxelize_sphere_test.png", 0.f);
    } else {
        std::cout << "voxelize error: " << voxel_db_->error_message() << std::endl;
    }

    EXPECT_TRUE(results);
}

TEST_F(VoxelDatabaseFullTest, VoxelizeBox) {
    voxel_db_->initalize_database({256, 256, 256}, {-2.f, -2.f, -2.f}, {4.f, 4.f, 4.f});

    bool results = voxel_db_->voxelize_cuboid(VoxelOperation::kOverwrite, {0.f, 0.f, 0.f}, {1.f, 0.75f, 0.5f});

    if (results) {
        EXPECT_LT(voxel_db_->sdf_at_point({-1.f, 0.f, 0.f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({1.f, 0.f, 0.f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, -0.75f, 0.f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, 0.75f, 0.f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, 0.f, -0.5f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, 0.f, 0.5f}), voxel_db_->get_tolerance());

        WriteToImageRGB("voxelize_box_test.png", 0.f);
    } else {
        std::cout << "voxelize error: " << voxel_db_->error_message() << std::endl;
    }

    EXPECT_TRUE(results);
}

TEST_F(VoxelDatabaseFullTest, VoxelizeTorus) {
    voxel_db_->initalize_database({256, 256, 256}, {-2.f, -2.f, -2.f}, {4.f, 4.f, 4.f});

    bool results = voxel_db_->voxelize_torus(VoxelOperation::kOverwrite, {0.f, 0.f, 0.f}, 1.2f, 0.3f);

    if (results) {
        EXPECT_LT(voxel_db_->sdf_at_point({-0.9f, 0.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.9f, 0.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, -0.9f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, 0.9f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({-1.5f, 0.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({1.5f, 0.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, -1.5f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, 1.5f, 0.0f}), voxel_db_->get_tolerance());

        WriteToImageRGB("voxelize_torus_test.png", 0.0f);
    } else {
        std::cout << "voxelize error: " << voxel_db_->error_message() << std::endl;
    }

    EXPECT_TRUE(results);
}

TEST_F(VoxelDatabaseFullTest, VoxelizeSphereBoxIntersect) {
    voxel_db_->initalize_database({256, 256, 256}, {-2.f, -2.f, -2.f}, {4.f, 4.f, 4.f});

    bool results1 = voxel_db_->voxelize_sphere(VoxelOperation::kOverwrite, {0.0f, 0.0f, 0.0f}, 1.f);
    bool results2 = voxel_db_->voxelize_cuboid(VoxelOperation::kIntersection, {0.f, 0.f, 0.f}, {0.9f, 0.9f, 0.9f});

    if (results1 && results2) {
        EXPECT_LT(voxel_db_->sdf_at_point({-0.9f, 0.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.9f, 0.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, -0.9f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, 0.9f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, 0.f, -0.9f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, 0.f, 0.9f}), voxel_db_->get_tolerance());

        WriteToImageRGB("voxelize_sphere_box_intersect.png", 0.f);
    } else {
        std::cout << "voxelize error: " << voxel_db_->error_message() << std::endl;
    }

    EXPECT_TRUE(results1 && results2);
}
TEST_F(VoxelDatabaseFullTest, VoxelizeSphereBoxUnion) {
    voxel_db_->initalize_database({256, 256, 256}, {-2.f, -2.f, -2.f}, {4.f, 4.f, 4.f});

    bool results1 = voxel_db_->voxelize_sphere(VoxelOperation::kOverwrite, {0.5f, -0.5f, -0.5f}, 1.f);
    bool results2 = voxel_db_->voxelize_cuboid(VoxelOperation::kUnion, {-0.5f, 0.5f, 0.5f}, {1.f, 1.f, 1.f});

    if (results1 && results2) {
        EXPECT_LT(voxel_db_->sdf_at_point({-1.f, 0.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({1.f, 0.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, -1.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, 1.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, 0.f, -1.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, 0.f, 1.0f}), voxel_db_->get_tolerance());

        WriteToImageRGB("voxelize_sphere_box_union.png", 0.f);
    } else {
        std::cout << "voxelize error: " << voxel_db_->error_message() << std::endl;
    }

    EXPECT_TRUE(results1 && results2);
}

TEST_F(VoxelDatabaseFullTest, VoxelizeTorusBoxDifference) {
    voxel_db_->initalize_database({256, 256, 256}, {-2.f, -2.f, -2.f}, {4.f, 4.f, 4.f});

    bool results1 = voxel_db_->voxelize_torus(VoxelOperation::kOverwrite, {0.0f, 0.0f, 0.0f}, 1.4f, 0.4f);
    bool results2 = voxel_db_->voxelize_cuboid(VoxelOperation::kDifference, {1.0f, 0.0f, 0.0f}, {1.0, 0.8f, 0.4f});

    if (results1 && results2) {
        EXPECT_LT(voxel_db_->sdf_at_point({-1.f, 0.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_GT(voxel_db_->sdf_at_point({1.f, 0.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, -1.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, 1.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({-1.8f, 0.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_GT(voxel_db_->sdf_at_point({1.8f, 0.f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, -1.8f, 0.0f}), voxel_db_->get_tolerance());
        EXPECT_LT(voxel_db_->sdf_at_point({0.f, 1.8f, 0.0f}), voxel_db_->get_tolerance());

        WriteToImageRGB("voxelize_torus_box_difference.png", 0.f);
    } else {
        std::cout << "voxelize error: " << voxel_db_->error_message() << std::endl;
    }

    EXPECT_TRUE(results1 && results2);
}

}  // namespace voxels
