#include <voxels.hpp>

#include <utility>
#include <algorithm>

namespace voxels {
namespace {

    class BadSdfVoxelDatabase : public IVoxelDatabase {
        bool initalize_database(const Size3d32& database_size, const Point3d32& min_bounds,
                                const Dimensions3d32& dimensions) override;

        bool voxelize_sphere(VoxelOperation operation, const Vector3d32& translate, float radius) override;
        bool voxelize_cuboid(VoxelOperation operation, const Vector3d32& translate,
                             const Dimensions3d32& half_size) override;
        bool voxelize_torus(VoxelOperation operation, const Vector3d32& translate, float major, float minor) override;

        float sdf_at_point(const Point3d32&) override;
    };

    // TODO: create data structure and set the following IVoxelDatabase private member variables.
    // float tolerance_  - Set tolerance_ to an appropriate value based on voxel_dimensions_
    // Size3d32 size_
    // Point3d32 min_corner_
    // Dimensions3d32 dimensions_
    // Dimensions3d32 voxel_dimensions_
    bool BadSdfVoxelDatabase::initalize_database(const Size3d32& database_size, const Point3d32& min_bounds,
                                                 const Dimensions3d32& dimensions) {
        // Clearly we can do better...
        error_message("Unimplemented algorithm.");
        return false;
    }

    bool BadSdfVoxelDatabase::voxelize_sphere(VoxelOperation operation, const Vector3d32& translate, float radius) {
        error_message("Unimplemented algorithm.");
        return false;
    }
    bool BadSdfVoxelDatabase::voxelize_cuboid(VoxelOperation operation, const Vector3d32& translate,
                                              const Dimensions3d32& half_size) {
        error_message("Unimplemented algorithm.");
        return false;
    }
    bool BadSdfVoxelDatabase::voxelize_torus(VoxelOperation operation, const Vector3d32& translate, float major,
                                             float minor) {
        error_message("Unimplemented algorithm.");
        return false;
    }

    float BadSdfVoxelDatabase::sdf_at_point(const Point3d32&) {
        error_message("Unimplemented algorithm.");
        return std::numeric_limits<float>().infinity();
    }

}  // namespace

std::unique_ptr<IVoxelDatabase> IVoxelDatabase::create() {
    return std::make_unique<BadSdfVoxelDatabase>();
}

std::string IVoxelDatabase::error_message() const noexcept {
    return error_message_;
}

void IVoxelDatabase::error_message(std::string error_message) noexcept {
    error_message_ = std::move(error_message);
}

}  // namespace voxels
