#ifndef VOXELS_HPP_
#define VOXELS_HPP_

#include <memory>
#include <string>
#include <vector>
#include <cmath>

namespace voxels {

// Voxel Database Interface.
//
// A voxel is a subdivision of space defined by an axis-aligned cuboid.  A Voxel Database is a collection of
// voxels which define a 3-dimensional object and associated metadata for each voxel.  The homework task is to build a
// voxel database that implements the interfaces defined by the class IVoxelDatabase.  This will include implementing
// implicit functions which are used to create 3D-objects and to populate the voxel database with SDF values.  A voxel's SDF point
// sample is the distance from the voxel's center to the closest position on the surface of the implicit geometry.
// The values are positive when outside the geometry, negative when inside the geometry and zero at the surface.
enum class VoxelOperation { kOverwrite, kUnion, kDifference, kIntersection };

struct Point3d32 {
    Point3d32(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {}
    Point3d32() : x(0.f), y(0.f), z(0.f) {}
    Point3d32(const Point3d32&) = default;
    Point3d32(Point3d32&&) = default;
    Point3d32& operator=(const Point3d32&) = default;
    Point3d32& operator=(Point3d32&&) = default;

    [[nodiscard]] float get_distance(const Point3d32& a) const {
        Point3d32 d(x - a.x, y - a.y, z - a.z);
        return std::sqrt(d.x * d.x + d.y * d.y + d.z * d.z);
    }

    float x;
    float y;
    float z;
};

struct Vector3d32 {
    Vector3d32(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {}
    Vector3d32() : x(0.f), y(0.f), z(0.f) {}
    Vector3d32(const Vector3d32&) = default;
    Vector3d32(Vector3d32&&) = default;
    Vector3d32& operator=(const Vector3d32&) = default;
    Vector3d32& operator=(Vector3d32&&) = default;

    [[nodiscard]] Point3d32 to_point_3d() const {
        return Point3d32(x, y, z);
    }

    float x;
    float y;
    float z;
};

struct Dimensions3d32 {
    Dimensions3d32(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {}
    Dimensions3d32() : x(0.f), y(0.f), z(0.f) {}
    Dimensions3d32(const Dimensions3d32&) = default;
    Dimensions3d32(Dimensions3d32&&) = default;
    Dimensions3d32& operator=(const Dimensions3d32&) = default;
    Dimensions3d32& operator=(Dimensions3d32&&) = default;

    float x;
    float y;
    float z;
};

struct Size3d32 {
    Size3d32(int _x, int _y, int _z) : x(_x), y(_y), z(_z) {}
    Size3d32() : x(0), y(0), z(0) {}
    Size3d32(const Size3d32&) = default;
    Size3d32(Size3d32&&) = default;
    Size3d32& operator=(const Size3d32&) = default;
    Size3d32& operator=(Size3d32&&) = default;

    [[nodiscard]] int product() const {
        return x * y * z;
    }

    int x;
    int y;
    int z;
};

struct Index3d32 {
    Index3d32(int _x, int _y, int _z) : x(_x), y(_y), z(_z) {}
    Index3d32() : x(0), y(0), z(0) {}
    Index3d32(const Index3d32&) = default;
    Index3d32(Index3d32&&) = default;
    Index3d32& operator=(const Index3d32&) = default;
    Index3d32& operator=(Index3d32&&) = default;

    int x;
    int y;
    int z;
};

class IVoxelDatabase {
public:
    virtual ~IVoxelDatabase() = default;

    // Create the vdb (voxel database) data structures and initalizes the member variables
    // tolerance_, size_, min_corner_, voxel_dimensions_.
    //
    // Parameters:
    //    database_size - voxel database size in voxels
    //    min_corner - minimum corner of the volume in world space
    //    voxel_dimension - voxel 3D dimensions in world units
    // The voxel database is used to store SDF (signed distance field) values for the center of each voxel.
    virtual bool initalize_database(const Size3d32& database_size, const Point3d32& min_corner,
                                    const Dimensions3d32& voxel_dimensions) = 0;

    // Sphere voxelization using the implicit geometry definition of a radius centered at the origin (0,0,0).
    // Parameters:
    //    radius - in world units
    //    translate - defines where the object should be place in world units
    //    operation - enum defining the boolean operation to be performed with the exising voxels in the database.
    virtual bool voxelize_sphere(VoxelOperation operation, const Vector3d32& translate, float radius) = 0;

    // Cuboid voxelization using the implicit geometry definition of the cuboid centered at the origin (0,0,0).
    // Parameters:
    //    half_size - dimensions of the cuboid.
    //    translate - defines where the object should be place in world space
    //    operation - enum defining the boolean operation to be performed with the exising voxels in the database.
    virtual bool voxelize_cuboid(VoxelOperation operation, const Vector3d32& translate,
                                 const Dimensions3d32& half_size) = 0;

    // Torus voxelization using the implicit geometry definition of major radius and minor radius centered at the origin
    // (0,0,0). The torus is oriented about the z-plane. Parameters:
    //    major - torus major radius in world units
    //    minor - torus minor radius in world units
    //    translate - defines where the object should be place in world space
    //    operation - enum defining the boolean operation to be performed with the exising voxels in the database.
    virtual bool voxelize_torus(VoxelOperation operation, const Vector3d32& translate, float major, float minor) = 0;

    // Returns the current SDF value at the given point.
    // Parameter:
    //    pos - position falling within a voxel used to retrieve its SDF value
    virtual float sdf_at_point(const Point3d32& pos) = 0;

    [[nodiscard]] float get_tolerance() const {
        return tolerance_;
    }
    [[nodiscard]] Size3d32 get_size() const {
        return size_;
    }
    [[nodiscard]] Point3d32 get_min_corner() const {
        return min_corner_;
    }
    [[nodiscard]] Dimensions3d32 get_dimensions() const {
        return dimensions_;
    }
    [[nodiscard]] Dimensions3d32 get_voxel_dimensions() const {
        return voxel_dimensions_;
    }

    // Returns the implementation of the IVoxelDatabase to be used.
    [[nodiscard]] static std::unique_ptr<IVoxelDatabase> create();

    std::string error_message() const noexcept;

protected:
    void error_message(std::string error_message) noexcept;

    // Tolerance is a distance measure -- when two points are as close, or closer than, tolerance_ apart in all
    // dimensions, then they are considered the same point.
    float tolerance_ = 0.0f;
    // Size of the voxel database in voxels
    Size3d32 size_ = {0, 0, 0};
    // Minimum corner of the voxel database in world space
    Point3d32 min_corner_ = {0.0f, 0.0f, 0.0f};
    // Voxel database dimensions in world units 
    Dimensions3d32 dimensions_ = {0.0f, 0.0f, 0.0f};
    // Voxel dimensions in world units
    Dimensions3d32 voxel_dimensions_ = {0.0f, 0.0f, 0.0f};
    // An error message describing what, if anything, went wrong with the most recent call to member function().
    std::string error_message_;
};

}  // namespace voxels

#endif
