#ifndef PIXEL_FORMATS_HPP_
#define PIXEL_FORMATS_HPP_


namespace voxels {

    struct PNG_IO_GREY8 {
        unsigned char g;
    };

    struct PNG_IO_GREY_ALPHA8 {
        unsigned char g;
        unsigned char a;
    };

    struct PNG_IO_RGB {
        unsigned char r;
        unsigned char g;
        unsigned char b;
    };

    struct PNG_IO_RGBA {
        unsigned char r;
        unsigned char g;
        unsigned char b;
        unsigned char a;
    };



}  // namespace voxels

#endif
