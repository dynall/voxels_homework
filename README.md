# Voxels Homework

**Basic 3D Geometry Coding**

We feel a short homework assignment will allow us to get a feel for your coding abilities and gives you an example of what kind of work we do at Dyndrite. To be clear, this assignment has already been implemented several times in our own code base by many members of the team. We expect it to take no more than 5 hours to complete.

You will implement a voxel database class and a voxelization algorithm for three implicit geometry definitions (sphere, rectangular prism, and torus).  The voxelization will store SDF (signed distance field) values for the individual voxels in the database.

Please see the following wikipedia articles for more details on voxel databases and geometric modeling:

* https://en.wikipedia.org/wiki/Voxel

* https://en.wikipedia.org/wiki/Signed_distance_function

* https://en.wikipedia.org/wiki/Constructive_solid_geometry


**Instructions**

We have provided a bitbucket repository with all files needed to complete this assignment. This includes an interface for the voxel database (in voxels.hpp and voxels.cpp), as well as some basic code to read and write png files (in file_png.h and file_png.cpp). It also includes a test framework with some tests and example input files to get you started.

We would like you to implement the voxel database by sub classing the interface class, and creating the data structure and voxelization functions. There are TODO comments with details throughout the code. The interface methods to be implemented have detailed comments in voxels.hpp. Tests have been defined to help you test your solutions and provide feedback during your implementation.  Several tests will write out PGN files showing the SDF values at a specific z-value in the database.  These PNG files will be helpful in debugging your implementation.
Feel free to add tests and extend classes as you see fit.


***The repository is located here:***

* https://bitbucket.org/dynall/voxels_homework/src/master/

We will test your implementation locally with a different set of test files that may contain errors and interesting edge cases. You will want to indicate any algorithmic decisions you've made via comments in the code. During the in-person interview, we will also do a code review with you to check out readability, efficiency, and robustness. If we have any issues compiling/running your code, we will reach out to you to ensure we resolve those issues.

***Turning in your homework:***

Please email us your solution as a zip file and/or provide a link to download it. You can email us at **eng.careers@dyndrite.com**.
